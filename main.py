from googletrans import Translator
from langs import LANGUAGES
from os import system as sys
import random
from tqdm import tqdm
import json
import argparse
import textwrap


def clear():
    sys('cls')


translator = Translator()

toCrush = ""
user_language = 'en'
running = True
used_langs = []
text_stages = []
crushed = ""

stageData = []


def addStageData(stage, text, lang):
    stageData.append({
        "stage": stage,
        "text": text,
        "lastLanguage": lang
    })


ap = argparse.ArgumentParser()
ap.add_argument("-q", "--quiet", required=False, action='store_true',
                help="Deactivate output")
ap.add_argument("-f", "--file", required=False, default="input.txt",
                help="The file where the source should come from")
ap.add_argument("-i", "--iterations", required=False, default=10, type=int,
                help="How often the text should be mangled")
ap.add_argument("-I", "--input", required=False,
                help="If you want to use a stream input, put the text here")
ap.add_argument("-j", "--json", required=False, action='store_true',
                help="Set output type to json. Will output text of every stage to a JSON-file")
ap.add_argument("-s", "--stream", required=False, action='store_true',
                help="Output the finished text to commandline to further process with other programs")
ap.add_argument("-t", "--txt", required=False, action='store_true',
                help="Set output type to txt. Will output the translation to a TXT-file")
ap.add_argument("-F", "--outputFile", required=False, default="output",
                help="If an output was defined this is it's filename without Extension. The file Extension will be added automatically")
args = ap.parse_args()


def outputPrint(text):
    if not args.quiet:
        print(text)


def view(stage):
    outputPrint("""\
    Used language: {}
    ------------------------
    {}""".format(LANGUAGES[used_langs[stage]], text_stages[stage]))


if __name__ == "__main__":
    toCrush = args.input
    while running:
        if toCrush == "" or toCrush is None:
            f = open(args.file, "r")
            toCrush = f.read()
            if toCrush == "" or toCrush is None:
                toCrush = input("Which sentence(s) do you want to rape?\n")
                outputPrint()

        user_language = (translator.detect(toCrush)).lang
        outputPrint(f"Detected language: {LANGUAGES[user_language]}\n")

        crushed = toCrush
        for i in tqdm(range(args.iterations), disable=args.quiet):

            lang = random.choice(list(LANGUAGES))
            used_langs.append(lang)

            try:
                crushed = translator.translate(crushed, dest=lang).text
                crushed = translator.translate(crushed, dest=user_language).text
                text_stages.append(crushed)
                if args.json:
                    addStageData(i, crushed, lang)

            except Exception as e:
                log = open("exceptions.log", "w")
                log.write("ERROR: {}\n\tLanguage: {}\n\tIteration: {}\n".format(e, lang, i))
                log.close()
                continue

        crushed = (translator.translate(crushed, dest=user_language)).text

        outputPrint("The result is:\n{}".format(crushed))

        if args.txt:
            with open("{}.txt".format(args.outputFile), "w") as f:
                f.write(crushed)
                f.write("\n")
                f.close()

        if args.json:
            with open("{}.json".format(args.outputFile), "w") as f:
                additionalInfo = {
                    "input": toCrush,
                    "startLanguage": user_language,
                    "iterations": args.iterations,
                    "stages": stageData
                }
                json.dump(additionalInfo, f)
                f.close()

        if args.stream:
            print(crushed)

        if args.quiet or args.stream:
            running = False
            break

        reviewing = True
        while reviewing:
            view_stage = input("\nWhich iteration do you want to view? (None --> Enter)    ")
            if view_stage == "":
                reviewing = False
            elif view_stage.isdecimal():
                if 0 <= int(view_stage) < args.iterations:
                    view(int(view_stage))
                elif int(view_stage) > args.iterations:
                    outputPrint(textwrap.dedent("""\
                        Entered value was too high!
                        Please enter a number between 0 and {}\
                    """.format(args.iterations)))
            else:
                outputPrint("Nothing valid entered! Assuming you don't want anything")
                reviewing = False

        if args.input is not None or args.json or args.txt:
            running = False
            break

        if input("\nDo you want to crush another one? (y/n)\t") == "y":
            toCrush = ""
        else:
            running = False
